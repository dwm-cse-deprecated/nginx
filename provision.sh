#! /bin/bash

set -ex

yum -y install tar
yum -y install make gcc-c++
yum -y install pcre pcre-devel openssl openssl-devel

cd /usr/local/src
curl -L http://nginx.org/download/nginx-1.8.0.tar.gz -o nginx-1.8.0.tar.gz
tar zxvf nginx-1.8.0.tar.gz

cd nginx-1.8.0

./configure \
--user=nginx \
--group=nginx \
--prefix=/usr/local/nginx \
--sbin-path=/usr/local/nginx/sbin/nginx \
--conf-path=/usr/local/nginx/etc/nginx.conf \
--pid-path=/var/run/nginx.pid \
--lock-path=/var/run/nginx.lock \
--error-log-path=/var/log/nginx/error_log \
--http-log-path=/var/log/nginx/access_log \
--http-client-body-temp-path=/var/cache/nginx/client_temp \
--http-proxy-temp-path=/var/cache/nginx/proxy_temp \
--http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
--http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
--http-scgi-temp-path=/var/cache/nginx/scgi_temp \
--with-http_realip_module \
--with-http_ssl_module \
--without-http_charset_module \
--without-http_gzip_module \
--without-http_ssi_module \
--without-http_userid_module \
--without-http_autoindex_module \
--without-http_geo_module \
--without-http_map_module \
--without-http_split_clients_module \
--without-http_uwsgi_module \
--without-http_scgi_module \
--without-http_limit_conn_module \
--without-http_empty_gif_module \
--without-http_browser_module \
--without-http_upstream_ip_hash_module

make && make install

groupadd nginx
useradd -g nginx nginx

mkdir -p /var/cache/nginx
chown -R nginx:nginx /var/cache/nginx

echo "export PATH=\$PATH:/usr/local/nginx/sbin" > /etc/profile.d/nginx.sh

cp /tmp/nginx/configs/init.d /etc/rc.d/init.d/nginx
cp /tmp/nginx/configs/logrotate /etc/logrotate.d/nginx
cp /tmp/nginx/configs/conf /usr/local/nginx/etc/nginx.conf

mkdir /usr/local/nginx/etc/conf.d

chmod 755 /etc/rc.d/init.d/nginx
chkconfig --add nginx
